package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aleksandr on 02/25/2016.
 */

@Repository
public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long>, QueryDslPredicateExecutor<MaintenancePlan> {

}
