@SuppressWarnings("deprecation")
@Service
public class HibernateBasedIdentifierGenerator {
    SessionFactory sessionFactory;
    Dialect dialect;
    Map<String, SequenceGenerator> generators = new HashMap<>();
    ObjectNameNormalizer nameNormalizer;

    @Autowired
    public HibernateBasedIdentifierGenerator(EntityManagerFactory emf) throws SQLException {
        sessionFactory =  emf.unwrap(SessionFactory.class);
        dialect = ((SessionFactoryImpl)sessionFactory).getDialect();

        nameNormalizer = new ObjectNameNormalizer() {
            Configuration conf = new Configuration();
            protected boolean isUseQuotedIdentifiersGlobally() { return false; }
            protected NamingStrategyDelegator getNamingStrategyDelegator() { return conf.getNamingStrategyDelegator(); }
            protected NamingStrategy getNamingStrategy() { return conf.getNamingStrategy(); }
        };
    }

    public Long getID(final String sequenceName) {
        SequenceGenerator generator = generators.get(sequenceName);
        if (generator == null) {
            Properties params = new Properties();
            params.setProperty("sequence", sequenceName);
            params.put("identifier_normalizer", nameNormalizer);
            generator = new SequenceGenerator();
            generator.configure(LongType.INSTANCE, params, dialect);
            generators.put(sequenceName, generator);
        }
        return (Long) generator.generate((SessionImpl)sessionFactory.openSession(), null);
    }
}