package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPlantInventoryItem is a Querydsl query type for PlantInventoryItem
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlantInventoryItem extends EntityPathBase<PlantInventoryItem> {

    private static final long serialVersionUID = -1138619561L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlantInventoryItem plantInventoryItem = new QPlantInventoryItem("plantInventoryItem");

    public final EnumPath<EquipmentCondition> equipment_condition = createEnum("equipment_condition", EquipmentCondition.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QMaintenancePlan mplant;

    public final QMaintainanceTask mReservation;

    public final QPlantInventoryEntry pEntry;

    public final StringPath serial_number = createString("serial_number");

    public QPlantInventoryItem(String variable) {
        this(PlantInventoryItem.class, forVariable(variable), INITS);
    }

    public QPlantInventoryItem(Path<? extends PlantInventoryItem> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantInventoryItem(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantInventoryItem(PathMetadata<?> metadata, PathInits inits) {
        this(PlantInventoryItem.class, metadata, inits);
    }

    public QPlantInventoryItem(Class<? extends PlantInventoryItem> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.mplant = inits.isInitialized("mplant") ? new QMaintenancePlan(forProperty("mplant")) : null;
        this.mReservation = inits.isInitialized("mReservation") ? new QMaintainanceTask(forProperty("mReservation"), inits.get("mReservation")) : null;
        this.pEntry = inits.isInitialized("pEntry") ? new QPlantInventoryEntry(forProperty("pEntry"), inits.get("pEntry")) : null;
    }

}

