package sales.web.dto;

import lombok.Data;

/**
 * Created by lgbanuelos on 26/02/16.
 */
@Data
public class QueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}
