package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPlantInventoryEntry is a Querydsl query type for PlantInventoryEntry
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlantInventoryEntry extends EntityPathBase<PlantInventoryEntry> {

    private static final long serialVersionUID = -941326162L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlantInventoryEntry plantInventoryEntry = new QPlantInventoryEntry("plantInventoryEntry");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final QPlantInventoryItem pItem;

    public final QMaintenancePlan plantSpec;

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final QBusinessPeriod rentalPeriod;

    public QPlantInventoryEntry(String variable) {
        this(PlantInventoryEntry.class, forVariable(variable), INITS);
    }

    public QPlantInventoryEntry(Path<? extends PlantInventoryEntry> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantInventoryEntry(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantInventoryEntry(PathMetadata<?> metadata, PathInits inits) {
        this(PlantInventoryEntry.class, metadata, inits);
    }

    public QPlantInventoryEntry(Class<? extends PlantInventoryEntry> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.pItem = inits.isInitialized("pItem") ? new QPlantInventoryItem(forProperty("pItem"), inits.get("pItem")) : null;
        this.plantSpec = inits.isInitialized("plantSpec") ? new QMaintenancePlan(forProperty("plantSpec")) : null;
        this.rentalPeriod = inits.isInitialized("rentalPeriod") ? new QBusinessPeriod(forProperty("rentalPeriod"), inits.get("rentalPeriod")) : null;
    }

}

