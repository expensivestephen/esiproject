package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QBusinessPeriod is a Querydsl query type for BusinessPeriod
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBusinessPeriod extends EntityPathBase<BusinessPeriod> {

    private static final long serialVersionUID = 1824274580L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBusinessPeriod businessPeriod = new QBusinessPeriod("businessPeriod");

    public final DatePath<java.time.LocalDate> endDate = createDate("endDate", java.time.LocalDate.class);

    public final SimplePath<javax.persistence.OneToMany> MaintainanceTask = createSimple("MaintainanceTask", javax.persistence.OneToMany.class);

    public final QPlantReservation rental_period;

    public final DatePath<java.time.LocalDate> startDate = createDate("startDate", java.time.LocalDate.class);

    public QBusinessPeriod(String variable) {
        this(BusinessPeriod.class, forVariable(variable), INITS);
    }

    public QBusinessPeriod(Path<? extends BusinessPeriod> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBusinessPeriod(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBusinessPeriod(PathMetadata<?> metadata, PathInits inits) {
        this(BusinessPeriod.class, metadata, inits);
    }

    public QBusinessPeriod(Class<? extends BusinessPeriod> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.rental_period = inits.isInitialized("rental_period") ? new QPlantReservation(forProperty("rental_period"), inits.get("rental_period")) : null;
    }

}

