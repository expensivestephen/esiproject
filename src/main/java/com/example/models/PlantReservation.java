package com.example.models;


import javax.persistence.Embedded;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class PlantReservation {
	@Id @GeneratedValue
	Long id;
	// Replace startDate & endDate with a BusinessPeriod
	@Embedded
	BusinessPeriod rental_period;
	
	@OneToMany
	MaintainanceTask plantReservation;
	
	@OneToOne
	BusinessPeriod schedule;
	
	@OneToOne
	PurchaseOrder rental;

}
