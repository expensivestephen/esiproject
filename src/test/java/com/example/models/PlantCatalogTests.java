package com.example.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.Demo3Application;
import com.example.EsiProjectApplication;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

//import static org.hamcrest.Matchers.x;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EsiProjectApplication.class) 
@Sql(scripts="plants-datasets.sql")
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantCatalogTests {
	@Autowired
	PlantInventoryEntryRepository repo;
	
	@Test
	public void queryPlantCatalog() {
		assertThat(repo.count(), is(14l));
	}
		
	@Test
	public void sanityCheck(){
		assertThat(repo.count(), is(14l));

	}
	
	@Test
	public void queryByName() {
		assertThat(repo.findByNameContaining("Mini").size(), is(2));
	}


	/*@Test
	public void findAvailableTest() {
		PlantInventoryEntry p = plantRepo.findOne(1l);

		assertThat(plantRepo.findAvailablePlants(LocalDate.of(2016,2,20), LocalDate.of(2016,2,25)), hasItem(p));

		PurchaseOrder po = new PurchaseOrder();
		po.setPlant(p);
    	po.setRentalPeriod(BusinessPeriod.of(LocalDate.of(2016, 2, 20), LocalDate.of(2016, 2, 25)));
		poRepo.save(po);

		assertThat(plantRepo.findAvailablePlants(LocalDate.of(2016,2,20), LocalDate.of(2016,2,25)), not(hasItem(p)));
	}*/
}
