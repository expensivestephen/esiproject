package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by Aleksandr on 02/25/2016.
 */
@Repository
public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTask, Long>, QueryDslPredicateExecutor<MaintenanceTask> {

}