package com.example.models;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long>, QueryDslPredicateExecutor<PlantInventoryEntry> {
	List<PlantInventoryEntry> findByNameContaining(String str);
	//List<PlantInventoryEntry> findAvailablePlants(LocalDate start_date, LocalDate end_date);
	
	@Query("select p from PlantInventoryEntry p where LOWER(p.name) like ?1")
	List<PlantInventoryEntry>finderMethod(String name);

}
