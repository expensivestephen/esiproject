package com.example.models;

import static com.example.models.InventorySpecifications.*;

import javafx.application.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Sql(scripts="plants-datasets.sql")
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantCatalogWithQueryDSLTests {
	@Autowired
	PlantInventoryEntryRepository plantRepo;

	/*@Test
	public void queryByName() {
		assertThat(plantRepo.findAll(nameContains("Mini").and(isAvailableFor(period))).size(), is(2));
	}*/
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@Sql(scripts="plants-dataset.sql")
//@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
}
