package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QMaintainanceTask is a Querydsl query type for MaintainanceTask
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMaintainanceTask extends EntityPathBase<MaintainanceTask> {

    private static final long serialVersionUID = -1020847054L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMaintainanceTask maintainanceTask = new QMaintainanceTask("maintainanceTask");

    public final QPlantInventoryItem pItem;

    public final QPlantReservation plantReservation;

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final QBusinessPeriod rental_period;

    public final EnumPath<TypeOfWork> type_of_work = createEnum("type_of_work", TypeOfWork.class);

    public QMaintainanceTask(String variable) {
        this(MaintainanceTask.class, forVariable(variable), INITS);
    }

    public QMaintainanceTask(Path<? extends MaintainanceTask> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMaintainanceTask(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMaintainanceTask(PathMetadata<?> metadata, PathInits inits) {
        this(MaintainanceTask.class, metadata, inits);
    }

    public QMaintainanceTask(Class<? extends MaintainanceTask> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.pItem = inits.isInitialized("pItem") ? new QPlantInventoryItem(forProperty("pItem"), inits.get("pItem")) : null;
        this.plantReservation = inits.isInitialized("plantReservation") ? new QPlantReservation(forProperty("plantReservation"), inits.get("plantReservation")) : null;
        this.rental_period = inits.isInitialized("rental_period") ? new QBusinessPeriod(forProperty("rental_period"), inits.get("rental_period")) : null;
    }

}

