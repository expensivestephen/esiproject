package com.example.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class PlantInventoryItem {
	@Id
	@GeneratedValue
	Long id;
	
	EquipmentCondition equipment_condition;
	String serial_number;
	
	@ManyToOne
	PlantInventoryEntry pEntry;
	
	@OneToOne
	MaintainanceTask mReservation;
	
	@OneToOne
	MaintenancePlan mplant;
}
