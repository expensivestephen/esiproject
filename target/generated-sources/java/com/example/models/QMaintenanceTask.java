package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QMaintenanceTask is a Querydsl query type for MaintenanceTask
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMaintenanceTask extends EntityPathBase<MaintenanceTask> {

    private static final long serialVersionUID = -1337994587L;

    public static final QMaintenanceTask maintenanceTask = new QMaintenanceTask("maintenanceTask");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final EnumPath<TypeOfWork> typeOfWork = createEnum("typeOfWork", TypeOfWork.class);

    public QMaintenanceTask(String variable) {
        super(MaintenanceTask.class, forVariable(variable));
    }

    public QMaintenanceTask(Path<? extends MaintenanceTask> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMaintenanceTask(PathMetadata<?> metadata) {
        super(MaintenanceTask.class, metadata);
    }

}

