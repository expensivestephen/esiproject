package sales.web;

import com.example.sales.web.dto.QueryDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by lgbanuelos on 26/02/16.
 */
@Controller
@RequestMapping("/dashboard")
public class SalesController {
    @RequestMapping(method = RequestMethod.GET, value="/catalog/form")
    public String getForm(Model model) {
        model.addAttribute("catalogQuery", new QueryDTO());
        return "dashboard/catalog/query-form";
    }

    @RequestMapping(method = RequestMethod.POST, value="/catalog/query")
    public String executeQuery(Model model, QueryDTO query) {
        System.out.println(query);
        return "dashboard/catalog/query-result";
    }
}
