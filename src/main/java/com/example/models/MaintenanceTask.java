package com.example.models;

import java.math.BigDecimal;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
public class MaintenanceTask {
	@Id
	@GeneratedValue
	Long id;
	
	  @Column(precision=8,scale=2)
	  BigDecimal price;

	  @Enumerated(EnumType.STRING)
	  TypeOfWork typeOfWork;

	  String description;
	  
}
