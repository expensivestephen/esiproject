package com.example.models;

import java.math.BigDecimal;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class MaintainanceTask {
	BigDecimal price;
	TypeOfWork type_of_work;
	
	@Embedded
	BusinessPeriod rental_period;
	
	@OneToOne
	PlantReservation plantReservation;
	
	@OneToOne
	PlantInventoryItem pItem;

}
