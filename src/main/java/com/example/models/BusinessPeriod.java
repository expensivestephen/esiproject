package com.example.models;

import java.time.LocalDate;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@Entity

@Embeddable
@Value
@NoArgsConstructor(force=true,access=AccessLevel.PRIVATE)
@AllArgsConstructor(staticName="of")
public class BusinessPeriod {
	LocalDate startDate;
	LocalDate endDate;
	
	OneToMany
	MaintainanceTask;
	
	@OneToOne
	PlantReservation rental_period;
}
