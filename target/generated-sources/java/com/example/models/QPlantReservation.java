package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPlantReservation is a Querydsl query type for PlantReservation
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlantReservation extends EntityPathBase<PlantReservation> {

    private static final long serialVersionUID = 595977876L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlantReservation plantReservation1 = new QPlantReservation("plantReservation1");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QMaintainanceTask plantReservation;

    public final QPurchaseOrder rental;

    public final QBusinessPeriod rental_period;

    public final QBusinessPeriod schedule;

    public QPlantReservation(String variable) {
        this(PlantReservation.class, forVariable(variable), INITS);
    }

    public QPlantReservation(Path<? extends PlantReservation> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantReservation(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantReservation(PathMetadata<?> metadata, PathInits inits) {
        this(PlantReservation.class, metadata, inits);
    }

    public QPlantReservation(Class<? extends PlantReservation> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.plantReservation = inits.isInitialized("plantReservation") ? new QMaintainanceTask(forProperty("plantReservation"), inits.get("plantReservation")) : null;
        this.rental = inits.isInitialized("rental") ? new QPurchaseOrder(forProperty("rental"), inits.get("rental")) : null;
        this.rental_period = inits.isInitialized("rental_period") ? new QBusinessPeriod(forProperty("rental_period"), inits.get("rental_period")) : null;
        this.schedule = inits.isInitialized("schedule") ? new QBusinessPeriod(forProperty("schedule"), inits.get("schedule")) : null;
    }

}

