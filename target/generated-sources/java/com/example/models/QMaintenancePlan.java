package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QMaintenancePlan is a Querydsl query type for MaintenancePlan
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QMaintenancePlan extends EntityPathBase<MaintenancePlan> {

    private static final long serialVersionUID = -1338103735L;

    public static final QMaintenancePlan maintenancePlan = new QMaintenancePlan("maintenancePlan");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QMaintenancePlan(String variable) {
        super(MaintenancePlan.class, forVariable(variable));
    }

    public QMaintenancePlan(Path<? extends MaintenancePlan> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMaintenancePlan(PathMetadata<?> metadata) {
        super(MaintenancePlan.class, metadata);
    }

}

